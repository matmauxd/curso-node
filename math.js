// funcion con nombre o nombrada
function suma(x, y) {
    return x + y;
}
// funcion flecha
var sumaFlecha = function (x, y) {
    return x + y;
};
// console.log(suma(1,2) );
// funcion con parametros opcionales
var funcionOpcionalSuma = function (x, y) {
    if (!y)
        return x;
    return x + y;
};
// console.log(funcionOpcionalSuma(1));
// console.log(funcionOpcionalSuma(1,2));
// Otra opcion es agregarle un valor por defecto
var funcionOpcionalSuma2 = function (x, y) {
    if (y === void 0) { y = 0; }
    return x + y;
};
// Funcion multiplicación
function multiplicación(x, y) {
    return x * y;
}
// Funcion potencia
function potencia(x, y) {
    return Math.pow(x, y);
}
console.log(funcionOpcionalSuma2(1));
console.log(funcionOpcionalSuma2(1, 2));
console.log(potencia(5, 2));
console.log(multiplicación(6, 6));
