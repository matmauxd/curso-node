import express from "express";
import * as productos from "./productos";

const app = express();

app.use(express.json());

app.get("/", function(request, response){
    response.send("Hello World");
})

app.get("/productos", function(request, response){
    response.send(productos.getStock());
})


app.post('/productos', function(request, response) {
    const body = request.body
    response.send(productos.storeProductos(body));
})

app.delete(`/productos/:id` , function(request, response) {
    const idProducto = Number(request.params.id)
    response.send(productos.deleteProductos(idProducto));
}) 




app.listen(3000, function(){

    console.info("servidor escuchando en http://localhost:3000 ")
})